## Synopsis

Java command line application for help in scheduling projects and assigning the right resources.

## Code Example

java -jar app.jar %path%/resources.txt %path%/projects.txt

## Motivation

Plan the resources this way, that usage of the people is optimized and projects are finished as early as possible.

## Installation

Java Runtime Environment must be installed. To start the program use terminal / shell with code above.


## License

MIT License 2017 by Reik Badelt